#!/bin/bash

if [ -f /etc/xdg/autostart/factory.desktop ];then
    grep "OnlyShowIn=LXDE;" /etc/xdg/autostart/factory.desktop > /dev/null
    if [ $? == 0 ];then
        echo 1-1
    else
        echo 0
    fi
    exit 0
fi

mode=`grep "Factory=" /usr/share/version/VersionInfo`
if [ x"$mode" != "x" ];then
    if [[ "$mode" == "Factory=1" ]];then
        echo 2-1
    else
        echo 0
    fi
    exit 0
fi
    

if [ -f /etc/xdg/autostart/itep-factoryupdate.desktop ];then
    grep "OnlyShowIn=LXDE;" /etc/xdg/autostart/itep-factoryupdate.desktop > /dev/null
    if [ $? == 0 ];then
        echo 3-1
    else
        echo 0
    fi
    exit 0
fi
