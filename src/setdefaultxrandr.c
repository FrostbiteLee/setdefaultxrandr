#include <stdio.h>  
#include <stdlib.h>  
#include <string.h>  
#include "setdefaultxrandr.h"
#include "savexrandr.h"

static char *userspath[USERCOUNTS];
static xRandrCurrent *xcurrent = NULL;//add xrandr history configure by lh
static char tcsn[20], tcproduct[20];//add tc model judgment

int parse_xrandr_command(char *command, MonInfo *info1, MonInfo *info2, MonInfo *info3)
{
	char *p, *ps;
	int count = 0;
	MonInfo *tmp = NULL;

	p = strtok_r(command, " ", &ps);
	while(p)
	{
		if(strcmp(p, "--output") == 0)
		{
			p = strtok_r(NULL, " ", &ps);
			if(p && strcmp(info1->monitor, "") == 0)
			{
				tmp = info1;
			}
			else if(p && strcmp(info2->monitor, "") == 0)
			{
				tmp = info2;
			}
			else if(p && strcmp(info3->monitor, "") == 0)
			{
				tmp = info3;
			}
			else
				return count;

			strncpy(tmp->monitor, p, sizeof(tmp->monitor));
			count++;
		}
		else if(strcmp(p, "--mode") == 0)
		{
			p = strtok_r(NULL, " ", &ps);
			if(  p
			&& strcmp(tmp->monitor, "") != 0)
			{
				strncpy(tmp->res, p, sizeof(tmp->res));
			}
		}
		else if(strcmp(p, "--off") == 0)
		{
			if(strcmp(tmp->monitor, "") != 0)
			{
				strncpy(tmp->res, "off", sizeof(tmp->res));
			}
		}
		p = strtok_r(NULL, " ", &ps);
	}
	return count;
}

int get_priority(char *monitor)
{
	if(strstr(monitor, "VGA") != 0) 
		return 1;
	else if(strstr(monitor, "HDMI") != 0) 
		return 2;
	else if(strstr(monitor, "DP") != 0) 
		return 3;
	return 0;
}

void
resolution_mode_to_wh(char *mode, char *w, char *h)
{
	char *strx = strstr(mode, "x");

	strncpy(w, mode, strlen(mode)-strlen(strx));
	strcpy(h, strx+1);
}

void init_monitor(MonInfo *moninfo)
{
	memset(moninfo->monitor, 0, sizeof(moninfo->monitor));
	memset(moninfo->res, 0, sizeof(moninfo->res));
	memset(moninfo->allres, 0, sizeof(moninfo->allres));
	moninfo->priority = 0;
}

void create_desktop(char *execbuf, char *configdir)
{
	char* dirname = NULL;
    	const char grp[] = "Desktop Entry";
    	GKeyFile* kf;
    	char* file, *data;
    	gsize len;
    
    	/* create user autostart dir */
    	dirname = g_build_filename(configdir/*g_get_user_config_dir()*/, "autostart", NULL);
    	g_mkdir_with_parents(dirname, 0700);
    	if(dirname != NULL) {g_free(dirname); dirname = NULL;}

    	kf = g_key_file_new();

    	g_key_file_set_string( kf, grp, "Type", "Application" );
    	g_key_file_set_string( kf, grp, "Name", _("LXRandR autostart") );
    	g_key_file_set_string( kf, grp, "Comment", _("Start xrandr with settings done in LXRandR") );
    	g_key_file_set_string( kf, grp, "Exec",  execbuf);//cmd->str
    	g_key_file_set_string( kf, grp, "OnlyShowIn", "LXDE" );

    	data = g_key_file_to_data(kf, &len, NULL);
    	file = g_build_filename(  configdir,
                              "autostart",
                              "lxrandr-autostart.desktop",
                              NULL );
    	/* save it to user-specific autostart dir */
    	g_file_set_contents(file, data, len, NULL);
    	if(kf != NULL) {g_key_file_free (kf);kf = NULL;}
    	if(file != NULL) {g_free(file);file = NULL;}
    	if(data != NULL) {g_free(data);data = NULL;}
}
void get_sh_path(char *path, char *homepath, char *shname)
{
	strcat(path, homepath);
	strcat(path, shname);
}
int save_xrandr(char *upath, const char *command)
{
	FILE *fp;
	char shpath[1024];
	char username[100];
	char cmd[1024];
	char configdir[100];
	int sysret;

	//newvirtualres.sh
	memset(shpath, 0, sizeof(shpath));
	get_sh_path(shpath, upath, SHPATH);
	printf("save_xrandrshpath:%s\n", shpath);
	
	fp = fopen(shpath, "w");
	if(fp == NULL) 
	{
		return 0;
	}
	fputs("#! /bin/bash\n", fp);
	fputs("#lxrandr\n", fp);
	fputs(command, fp);
	fclose(fp);
	printf("save_xrandrcommand:%s\n", command);

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "chmod +x %s", shpath);
	sysret = system(cmd);
	printf("save_xrandrcmd:%s\n", cmd);
	
	memset(username, 0, sizeof(username));
	strcpy(username, strrchr(upath, '/')+1);
	printf("save_xrandrusername:%s\n", username);
	
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "chown %s:%s %s", username, username, shpath);
	sysret = system(cmd);
	printf("save_xrandrcmd2:%s\n", cmd);
	//lxrandr.desktop
	memset(configdir, 0, sizeof(configdir));
	strcat(configdir, upath);
	strcat(configdir, "/.config");
	create_desktop(shpath, configdir);
	printf("configdir:%s\n", configdir);

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "chown %s:%s %s%s", username, username, upath, AUOSTARTPATH);
	sysret = system(cmd);
	printf("save_xrandrcmd3:%s\n", cmd);

	return 1;
}
void init_userspath()
{
	int i = 0;
	for( ; i < USERCOUNTS ; i++)
		userspath[i] = NULL;
}
int get_userspath()
{
	userspath[0] = (char *)malloc(sizeof(char)*1024);
	strcpy(userspath[0], "/root");

	FILE *fp = NULL;
	char output[1024];
	memset(output, 0, sizeof(output));
	int i = 1, iuseful = 0;
	fp = popen("cat /etc/passwd | awk -F: '$3>=1000'", "r");
	if(fp == NULL) 
		return 0;
	while(fgets(output, sizeof(output), fp))
	{printf("get_userspath output:%s\n", output);
		char *p, *ps;
		p = strtok_r(output, ":", &ps);
		while(p)
		{
			if(i == 3)
			{printf("3rd p:%s\n", p);
				if( (atoi(p) == 0)
				|| (atoi(p) >= 1000 && atoi(p) < 65534 )) 
					iuseful = 1;
				else 
					break;
			}
			else if(i == 6 && iuseful == 1)
			{
				int j;
				for( j = 1; j < USERCOUNTS; j++)
				{
					if(userspath[j] == NULL)
					{
						userspath[j] = (char *)malloc(sizeof(char)*1024);
						strcpy(userspath[j], p);
						iuseful = 0;
						break;
					}
				} 
				printf("userspath[j]:%s\n", userspath[j]);
				break;
			}	
			i++;
			p = strtok_r(NULL, ":", &ps);
		}
		memset(output, 0, sizeof(output));
		i = 1;
	}
	pclose(fp);
	return 1;
}
void free_userpath()
{
	int j;
	for( j = 0; j < USERCOUNTS; j++)
	{
		if(userspath[j] != NULL)
		{
			free(userspath[j]);
		}
	} 
}

int 
kf_get_startup_user_config_path(char *configpath)
{
	if(configpath == NULL)
	{
		return 0;
	}

	GKeyFile* kf;
	char grp[] = "SeatDefaults";
	gchar *str = NULL;

	kf = g_key_file_new();
	if(FALSE == g_key_file_load_from_file(kf, STARTUPLOCALUSERPATH, G_KEY_FILE_KEEP_COMMENTS, NULL))
	{
		if(FALSE == g_key_file_load_from_file(kf, STARTUPUSERPATH, G_KEY_FILE_KEEP_COMMENTS, NULL))
		{
			if(kf)
				g_key_file_free(kf);
			return 0;
		}
	}
	str = g_key_file_get_string(kf, grp, "autologin-user", NULL);
	if(str == NULL)
	{
		return 0;
	}
	if(strcmp(str, "root") == 0)
	{
		strcpy(configpath, "/root/.config");
	}
	else
	{
		sprintf(configpath, "/home/%s/.config", str);
	}

	if(kf)
		g_key_file_free(kf);
	return 1;
}

int
get_sh_xrandr_info2(char *userhomepath, MonInfo *info1, MonInfo *info2, MonInfo *info3)
{
	FILE *fp = NULL;
	char filename[100];
	char output[200];
	int count = 0, imark = 0;

	memset(filename, 0, sizeof(filename));
	snprintf(filename, sizeof(filename), "%s%s", userhomepath, SHPATH);
	fp = fopen(filename, "r");
	if(fp == NULL) 
		return 0;
	memset(output, 0, sizeof(output));
	while(fgets(output, sizeof(output), fp))
	{
		if(strstr(output, "#lxrandr") != NULL) 
		{
			imark = 1;
		}
		else if(  imark == 1 
			&& strstr(output, "xrandr --output") != NULL)
		{
			count += parse_xrandr_command(output, info1, info2, info3);
			printf("info1.monitor:%s\ninfo1.res:%s\ninfo2.monitor:%s\ninfo2.res:%s\ninfo3.monitor:%s\ninfo3.res:%s\n", 
		info1->monitor, info1->res, info2->monitor, info2->res, info3->monitor, info3->res);
		}
		memset(output, 0, sizeof(output));
	}

	fclose(fp);
	return count;
}

int 
match_current_sh(xRandrCurrent *current, MonInfo *info)
{
	if(current == NULL || info == NULL)
		return 0;

	xRandrCurrentMonitor *monitor;
	int i, match = 0;
	char mode[20];

	if(strcmp(current->monitor1->port, info->monitor) == 0)
	{
		monitor = current->monitor1;
	}
	else if(strcmp(current->monitor2->port, info->monitor) == 0)
	{
		monitor = current->monitor2;
	}
	else if(strcmp(current->monitor3->port, info->monitor) == 0)
	{
		monitor = current->monitor3;
	}
	else
	{
		return 0;
	}
	
	if(strcmp(info->res, "off") == 0)
	{
		return 1;
	}
	for(i = 0; i < monitor->nmode; i++)
	{
		if (i == 0 
		|| (i > 0 && (monitor->modes[i].width != monitor->modes[i - 1].width
		|| monitor->modes[i].height != monitor->modes[i - 1].height)))
		{
			memset(mode, 0, sizeof(mode));
			sprintf(mode, "%dx%d", monitor->modes[i].width, monitor->modes[i].height);
			if(strcmp(mode, info->res) == 0)
			{
				match = 1;
				break;
			}
		}
	}

	return match;	
}

int 
make_xrandr_cmd_record2(xRandrRecord *record, char *cmd)
{	
	if(!record || !cmd)
	{
		return 0;
	}

	xRandrCmd *Rcmd;
	char cmd1[300], cmd2[300];
	
	Rcmd = xRandrCmd_new();
	if(!Rcmd)
	{
		return 0;
	}

	memset(cmd1, 0, sizeof(cmd1));
	memset(cmd2, 0, sizeof(cmd2));
	if(  xRandrCmd_init(Rcmd, record->monitor1)
	&& xRandrCmd_init(Rcmd, record->monitor2)
	&& xRandrCmd_init(Rcmd, record->monitor3) )
	{
		strcat(cmd, "xrandr");
		if(strcmp(Rcmd->offmon1, "") != 0)
		{
			snprintf(cmd1, sizeof(cmd1), " --output %s --off\nxrandr", Rcmd->offmon1);
			if(strcmp(Rcmd->offmon2, "") != 0)
			{
				snprintf(cmd2, sizeof(cmd2), " --output %s --off\nxrandr", Rcmd->offmon2);
			}
			strcat(cmd, cmd1);
			strcat(cmd, cmd2);
		}
		memset(cmd1, 0, sizeof(cmd1));
		memset(cmd2, 0, sizeof(cmd2));
		if(strcmp(Rcmd->primon, "") != 0)
		{
			snprintf(cmd1, sizeof(cmd1), " --output %s --mode %s --rate 60.0 --rotation %s --primary --output %s --mode %s --rate 60.0 --rotation %s --right-of %s", 
					Rcmd->primon, Rcmd->prires, Rcmd->prirot,
					Rcmd->secmon1, Rcmd->secres1, Rcmd->secrot1, Rcmd->primon);
			strcat(cmd, cmd1);
		}
		else if(strcmp(Rcmd->secmon1, "") != 0)
		{
			snprintf(cmd1, sizeof(cmd1), " --output %s --mode %s --rate 60.0 --rotation %s",
					Rcmd->secmon1, Rcmd->secres1, Rcmd->secrot1);
			strcat(cmd, cmd1);
			if(strcmp(Rcmd->secmon2, "") != 0)
			{
				snprintf(cmd2, sizeof(cmd2), " --output %s --mode %s --rate 60.0 --rotation %s --same-as %s",
						Rcmd->secmon2, Rcmd->secres2, Rcmd->secrot2, Rcmd->secmon1);
				strcat(cmd, cmd2);
			}
			else
			{
				strcat(cmd, " --pos 0x0");
			}
		}
		else
		{
			printf("err\n");
			xRandrCmd_free(Rcmd);
			return 0;
		}
	}
	else
	{
		printf("err\n");
		xRandrCmd_free(Rcmd);
		return 0;
	}

	xRandrCmd_free(Rcmd);
	printf("make_xrandr_cmd_record2:%s\n", cmd);
	return 1;
}

void 
make_xrandr_cmd_record(xRandrRecord *record, char *cmd)
{	
	if(!record || !cmd)
	{
		return;
	}

	char cmd1[100], cmd2[100];
	
	memset(cmd1, 0, sizeof(cmd1));
	memset(cmd2, 0, sizeof(cmd2));
	strcpy(cmd, "xrandr");
	if(  strcmp(record->monitor1->manufacturer_code, "") != 0 
	&& record->monitor1->enable == 1)
	{
		snprintf(cmd1, sizeof(cmd1), " --output %s --mode %s --rate 60.0 --rotation %s", record->monitor1->port, 
														record->monitor1->mode,
														record->monitor1->rotation);
		if(  record->monitor1->primary == 1
		&& strcmp(record->monitor2->manufacturer_code, "") != 0  
		&& record->monitor2->enable == 1)
		{
			strcat(cmd1, " --primary");
		}
		else if(  record->monitor1->primary == 0
			&& strcmp(record->monitor2->manufacturer_code, "") == 0)
		{
			strcat(cmd1, " --pos 0x0");
		}
	}
	if(  strcmp(record->monitor2->manufacturer_code, "") != 0  
	&& record->monitor2->enable == 1)
	{
		snprintf(cmd2, sizeof(cmd2), " --output %s --mode %s --rate 60.0 --rotation %s", record->monitor2->port, 
														record->monitor2->mode,
														record->monitor2->rotation);
		if(record->monitor1->primary == 1)
		{
			strcat(cmd2, " --right-of ");
			strcat(cmd2, record->monitor1->port);
		}
		else if(record->monitor2->primary == 1)
		{
			strcat(cmd2, " --primary");
			strcat(cmd2, " --left-of ");
			strcat(cmd2, record->monitor1->port);
		}
		else if(  strcmp(record->monitor1->manufacturer_code, "") != 0 
			&& record->monitor1->enable == 1)
		{
			strcat(cmd2, " --same-as ");
			strcat(cmd2, record->monitor1->port);
		}
	}
	if(strcmp(cmd1, "") != 0)
	{
		strcat(cmd, cmd1);
	}
	if(strcmp(cmd2, "") != 0)
	{
		strcat(cmd, cmd2);
	}
	
	printf("make_xrandr_cmd_record:%s\n", cmd);
	return;
}

void
select_mon2(char *primary, char *secondary, char *port1, char *port2)
{
	if(!primary || !secondary || !port1 || !port2)
		return;

	int p1, p2;

	p1 = get_priority(port1);
	p2 = get_priority(port2);
	
	if(p1 > p2)
	{
		strcpy(primary, port1);
		strcpy(secondary, port2);
	}
	else
	{
		strcpy(primary, port2);
		strcpy(secondary, port1);
	}
}

void
select_mon3(char *primary, char *secondary, char *useless, char *port1, char *port2, char *port3)
{
	if(!primary || !secondary || !useless || !port1 || !port2 || !port3)
		return;

	int p1, p2, p3;

	p1 = get_priority(port1);
	p2 = get_priority(port2);
	p3 = get_priority(port3); 
	
	if(p1 > p2 && p1 > p3)
	{
		strcpy(primary, port1);
		if(p2 > p3)
		{
			strcpy(secondary, port2);
			strcpy(useless, port3);
		}
		else
		{
			strcpy(secondary, port3);
			strcpy(useless, port2);
		}
	}
	else if(p2 > p1 && p2 > p3)
	{
		strcpy(primary, port2);
		if(p1 > p3)
		{
			strcpy(secondary, port1);
			strcpy(useless, port3);
		}
		else
		{
			strcpy(secondary, port3);
			strcpy(useless, port1);
		}
	}
	else if(p3 > p1 && p3 > p2)
	{
		strcpy(primary, port3);
		if(p1 > p2)
		{
			strcpy(secondary, port1);
			strcpy(useless, port2);
		}
		else
		{
			strcpy(secondary, port2);
			strcpy(useless, port1);
		}
	}
}

void
make_xrandr_cmd_current(xRandrCurrent *current, char *cmd)
{
	char primon[10], secmon[10], offmon[10], prires[20], secres[20];
	char cmd0[100], cmd1[100], cmd2[100];
	
	memset(primon, 0, sizeof(primon));
	memset(secmon, 0, sizeof(secmon));
	memset(offmon, 0, sizeof(offmon));
	memset(prires, 0, sizeof(prires));
	memset(secres, 0, sizeof(secres));
	if(current->count == 1)
	{
		strcpy(primon, current->monitor1->port);
		get_suitable_resolution(current->monitor1, prires);
	}
	else if(current->count == 2)
	{
		select_mon2(primon, secmon, current->monitor1->port, current->monitor2->port);
		if(strcmp(primon, current->monitor1->port) == 0)
		{
			get_suitable_resolution(current->monitor1, prires);
			get_suitable_resolution(current->monitor2, secres);
		}
		else if(strcmp(primon, current->monitor2->port) == 0)
		{
			get_suitable_resolution(current->monitor2, prires);
			get_suitable_resolution(current->monitor1, secres);
		}
	}
	else if(current->count == 3)
	{
		select_mon3(primon, secmon, offmon, current->monitor1->port, current->monitor2->port, current->monitor3->port);
		if(strcmp(primon, current->monitor1->port) == 0)
		{
			get_suitable_resolution(current->monitor1, prires);
			if(strcmp(secmon, current->monitor2->port) == 0)
			{
				get_suitable_resolution(current->monitor2, secres);
			}
			else
			{
				get_suitable_resolution(current->monitor3, secres);
			}
		}
		else if(strcmp(primon, current->monitor2->port) == 0)
		{
			get_suitable_resolution(current->monitor2, prires);
			if(strcmp(secmon, current->monitor1->port) == 0)
			{
				get_suitable_resolution(current->monitor1, secres);
			}
			else
			{
				get_suitable_resolution(current->monitor3, secres);
			}
		}
		else if(strcmp(primon, current->monitor3->port) == 0)
		{
			get_suitable_resolution(current->monitor3, prires);
			if(strcmp(secmon, current->monitor1->port) == 0)
			{
				get_suitable_resolution(current->monitor1, secres);
			}
			else
			{
				get_suitable_resolution(current->monitor2, secres);
			}
		}
	}
	else
	{
		return;
	}

	strcat(cmd, "xrandr");
	//off first
	if(strcmp(offmon, "") != 0)
	{
		memset(cmd0, 0, sizeof(cmd0));
		sprintf(cmd0, " --output %s --off\nxrandr", offmon);
		strcat(cmd, cmd0);
	}
	//show
	memset(cmd1, 0, sizeof(cmd1));
	sprintf(cmd1, " --output %s --mode %s --rate 60.0 --rotation normal --primary", primon, prires);
	strcat(cmd, cmd1);
	if(  strcmp(secmon, "") != 0
	&& strcmp(secres, "") != 0)
	{
		memset(cmd2, 0, sizeof(cmd2));
		sprintf(cmd2, " --output %s --mode %s --rate 60.0 --rotation normal --right-of %s", secmon, secres, primon);
		strcat(cmd, cmd2);
	}
	else
	{
		strcat(cmd, " --pos 0x0");
	}

	printf("make_xrandr_cmd_current:%s\n", cmd);	
}

int 
check_factory_mode()
{
	FILE *fp = NULL;
	char output[50];
	int ret;

	fp = popen("/usr/bin/itep-check-factory-mode.sh", "r");
	if(fp == NULL)
	{ 
		return 0;
	}
	memset(output, 0, sizeof(output));
	if(fgets(output, sizeof(output), fp))
	{
		if(strstr(output, "1") != NULL)
		{
			ret = 1;
		}
		else
		{
			ret = 0;
		}
	}

	pclose(fp);
	return ret;
}

int my_popen(char *command, char *getbuf)
{
	FILE *fp;
	char tempbuf[1024];

	if((fp = popen(command, "r")) == NULL)
	{
		return 0;
	}
	memset(tempbuf, 0, sizeof(tempbuf));	
	while(fgets(tempbuf, sizeof(tempbuf), fp))
	{
		strcat(getbuf, tempbuf);
		memset(tempbuf, 0, sizeof(tempbuf));
	}
	
	pclose(fp);
	return 1;
}

int dmidecode_popen(char *command, char *getbuf)
{
	if(getbuf == NULL || command == NULL) 
		return 0;

	char info[100];
	memset(info, 0, sizeof(info));
	if(!my_popen(command, info)) 
		return 0;
	else if(strstr(info, "Permission denied") != 0) 
		return 0;
	else if(strstr(info, "O.E.M") != NULL || strstr(info, "o.e.m") != NULL || strcmp(info, "") == 0)
		return 0;

	strncpy(getbuf, info, strlen(info) -1);
	return 1;
}

static
void SERIALofSKU(char *str, char *serial, int len)
{
	if(str == NULL || serial == NULL)
		return;

	char *tmp = NULL;
	int ret;

	tmp = strstr(str, "SKU Number:");
	if(tmp)
	{
		strncpy(serial, tmp+strlen("SKU Number:")+1, len);
	}
	else
	{
		strcpy(serial, "unknown");
	}
}

static
int parse_serial(char *ser, char *sn, char *product)
{
	if(ser == NULL || sn == NULL || product == NULL)
		return 0;
	else
		ser[strlen(ser)] = '\0';

	char serial[11];
	int i = 0, SERIALLEN = 11;
	char *p = strtok(ser, "|");
	if(p) 
	{
		strncpy(serial, p, sizeof(serial));
		for (; i < SERIALLEN; i++)
		{
			if(serial[i] < '0' || serial[i] > '9')
				break;
		}
		if(i == SERIALLEN)
		{
			strcpy(sn, serial);
			p = strtok(NULL, "|");
			if(p && strlen(p) > 0)
			{
				strcpy(product, p);
				return 1;	
			}
		}
	}
	return 0;
}

int get_product_and_serial(char *sn, char *product)
{
	char str[100], serial[50];
	int ret;

	memset(str, 0, sizeof(str));
	memset(serial, 0, sizeof(serial));
	ret = dmidecode_popen("dmidecode -t system | grep SKU 2>&1", str);
	if (ret)
	{
		SERIALofSKU(str, serial, sizeof(serial));
		if(parse_serial(serial, sn, product))
		{
			return 1;
		}
	}
	memset(str, 0, sizeof(str));
	ret = dmidecode_popen("dmidecode -s system-serial-number 2>&1", str);
	if (ret)
	{
		if(parse_serial(str, sn, product))
		{
			return 1;
		}
	}
	if(strcmp(sn, "")  == 0)
		strcat(sn, _("Unknown"));
	if(strcmp(product, "")  == 0)
		strcat(product, _("Start TC"));

	return 0;
}

void record_product_and_serial(char *sn, char *product)
{
	if(!sn || !product)
		return;

	gchar* file, *data;
	gsize len;
	char *group_str = {"tcinformation"};
	char *filepath = {"/tmp/.tcinformation"};
	GKeyFile *gkeyfile = g_key_file_new();
	
	g_key_file_set_string(gkeyfile, group_str, "serial", sn); 	
	g_key_file_set_string(gkeyfile, group_str, "product", product); 	

	data = g_key_file_to_data(gkeyfile, &len, NULL);
	file = g_build_filename(filepath, NULL );
	g_file_set_contents(file, data, len, NULL);
    
	if(gkeyfile) 
		g_key_file_free (gkeyfile);
	if(file) 
		g_free(file);
	if(data) 
		g_free(data);
}

static int dp_is_hp(xRandrCurrent *current, char *monitor)
{
	if(!current)
		return -1;
	else if(current->monitor1)
	{
		if( (!strcmp("HWP", current->monitor1->manufacturer_code) || !strcmp("HPN", current->monitor1->manufacturer_code))
		&& strstr(current->monitor1->port, "DP"))
		{
			strcpy(monitor, current->monitor1->port);
			goto succ;
		}
	}
	else if(current->monitor2)
	{
		if( (!strcmp("HWP", current->monitor2->manufacturer_code) || !strcmp("HPN", current->monitor2->manufacturer_code))
		&& strstr(current->monitor2->port, "DP"))
		{
			strcpy(monitor, current->monitor2->port);
			goto succ;
		}
	}
	else if(current->monitor3)
	{
		if( (!strcmp("HWP", current->monitor3->manufacturer_code) || !strcmp("HPN", current->monitor3->manufacturer_code))
		&& strstr(current->monitor3->port, "DP"))
		{
			strcpy(monitor, current->monitor3->port);
			goto succ;
		}
	}
	return -1;
	
succ:
	printf("%s is HP monitor.\n", monitor);
	return 0;
}

static void modify_monitor_brightness(char *cmd, char *monitor)
{
	char command[1024];
	
	memset(command, 0, sizeof(command));
	sprintf(command, "\nxrandr --output %s --brightness %s\n", monitor, BRIGHTNESS);
	strcat(cmd, command);
	printf("Find %s, set brightness to %s...\n", monitor, BRIGHTNESS);

	return;
}

int main(int argc, char *argv[])
{
	if(check_factory_mode())
	{
		return 0;
	}
	memset(tcsn, 0, sizeof(tcsn));
	memset(tcproduct, 0, sizeof(tcproduct));
	if(get_product_and_serial(tcsn, tcproduct))
	{
		record_product_and_serial(tcsn, tcproduct);
	}
	init_userspath();
	get_userspath();
	if(userspath[0] == NULL) 
	{
		goto out;
	}

	xcurrent = xRandrCurrent_get_current_monitor();
	xrandr_current_monitor_print(xcurrent->monitor1);

	if( xcurrent == NULL 
	|| (xcurrent->count != 1 && xcurrent->count != 2 && xcurrent->count != 3))
	{
		printf("There is no monitor!!\n");
		goto out;
	}
	else 
	{
		xRandrRecord *xrecord = NULL, *matchrecord = NULL;
		int iuser;
		char cmd[1024];
		char mon1366[10];

		for(iuser = 0; iuser < USERCOUNTS; iuser++)
		{
			xrecord = xrandrrecord_new();
			if(userspath[iuser])
			{
				if(xrandr_read_record(userspath[iuser], xrecord) == 1)
				{
					matchrecord = match_current_record(xrecord, xcurrent);
				}
			}
			else
			{
				break;
			}
			printf("%d %s need to make xrandr cmd here!\n", xcurrent->count, userspath[iuser]);
			//make xrandr cmd
			memset(cmd, 0, sizeof(cmd));
			//set scaling mode if there is 1366x768
			memset(mon1366, 0, sizeof(mon1366));
			if(1 == check_has_1366(xcurrent->monitor1))
				strcpy(mon1366, xcurrent->monitor1->port);
			else if(1 == check_has_1366(xcurrent->monitor2))
				strcpy(mon1366, xcurrent->monitor2->port);
			else if(1 == check_has_1366(xcurrent->monitor3))
				strcpy(mon1366, xcurrent->monitor3->port);
			if(strcmp(mon1366, "") != 0)
				sprintf(cmd, "xrandr-bak --output %s --set \"scaling mode\" \"Full\"\n", mon1366);
			//set 1366x768 to VGA if there is "TC-9050X2A"
			if(!strcmp(tcproduct, "TC-9050X2A") && strcmp(mon1366, "") != 0)
			{
				strcat(cmd, "xrandr --addmode VGA1 1366x768 --output eDP1 --gamma 0.7:0.7:0.7\n");
			}

			if(matchrecord)
			{
				make_xrandr_cmd_record2(matchrecord, cmd);
			}
			else
			{
				make_xrandr_cmd_current(xcurrent, cmd);	
			}
			
#if MODIFYMONITOR
			char monitorname[10];
			if(strstr(cmd, "VGA") && !strstr(cmd, "VGA1 --off"))
				modify_monitor_brightness(cmd, "VGA1");
			memset(monitorname, 0, sizeof(monitorname));
			if(   strstr(cmd, "DP") 
			&& !strstr(cmd, "DP1 --off") && !strstr(cmd, "DP2 --off")
			&& dp_is_hp(xcurrent, monitorname) == 0)
				modify_monitor_brightness(cmd, monitorname);			
#endif			
			
			if(strcmp(cmd, "") != 0)
			{
				save_xrandr(userspath[iuser], cmd);
			}
			matchrecord = NULL;
			if (xrecord)
			{
				xrandrrecord_free(xrecord);
				xrecord = NULL;
			}
		}//FOR
		if (xrecord)
			xrandrrecord_free(xrecord);
	}
#if 0
	else if(xcurrent->count == 3)
	{
		MonInfo info1, info2, info3;
		int iuser;
		char cmd[1024];

		for(iuser = 0; iuser < USERCOUNTS; iuser++)
		{
			if(userspath[iuser])
			{
				init_monitor(&info1);
				init_monitor(&info2);
				init_monitor(&info3);
				if(get_sh_xrandr_info2(userspath[iuser], &info1, &info2, &info3) == 3)
				{
					if(  match_current_sh(xcurrent, &info1) == 1
					&& match_current_sh(xcurrent, &info2) == 1
					&& match_current_sh(xcurrent, &info3) == 1)
					{
						continue;
					}
				}
				printf("3 %s need to make xrandr cmd here!\n", userspath[iuser]);
				//make xrandr cmd
				memset(cmd, 0, sizeof(cmd));
				make_xrandr_cmd_current(xcurrent, cmd);	
				if(strcmp(cmd, "") != 0)
				{
					save_xrandr(userspath[iuser], cmd);
				}
			}
			else
			{
				break;
			}
		}
	}
#endif

out:
	free_userpath();
	if (xcurrent)
		xrandrcurrent_free(xcurrent);
	//if (x_randr)
        //	xrandr_free (x_randr);

	return 0;
}
