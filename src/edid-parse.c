/*
 * Copyright 2007 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * on the rights to use, copy, modify, merge, publish, distribute, sub
 * license, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* Author: Soren Sandmann <sandmann@redhat.com> */

/* Downloaded from <http://git.gnome.org/browse/gnome-desktop/tree/libgnome-desktop>
   (git commit 42452cada8cf1c4d7a81aded0a3ddbb5e97441de)
   Slightly modified by Lionel Le Folgoc <mrpouit@gmail.com> to build with full debug */

#include "edid.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <glib.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <ctype.h>

static const struct {
  int x, y, refresh, ratio_w, ratio_h;
  int hor_freq_hz, pixclk_khz, rb;
} established_timings3[] = {
  /* 0x06 bit 7 - 0 */
  {640, 350, 85, 64, 35, 37900, 31500},
  {640, 400, 85, 16, 10, 37900, 31500},
  {720, 400, 85, 9, 5, 37900, 35500},
  {640, 480, 85, 4, 3, 43300, 36000},
  {848, 480, 60, 53, 30, 31000, 33750},
  {800, 600, 85, 4, 3, 53700, 56250},
  {1024, 768, 85, 4, 3, 68700, 94500},
  {1152, 864, 75, 4, 3, 67500, 108000},
  /* 0x07 bit 7 - 0 */
  {1280, 768, 60, 5, 3, 47400, 68250, 1},
  {1280, 768, 60, 5, 3, 47800, 79500},
  {1280, 768, 75, 5, 3, 60300, 102250},
  {1280, 768, 85, 5, 3, 68600, 117500},
  {1280, 960, 60, 4, 3, 60000, 108000},
  {1280, 960, 85, 4, 3, 85900, 148500},
  {1280, 1024, 60, 5, 4, 64000, 108000},
  {1280, 1024, 85, 5, 4, 91100, 157500},
  /* 0x08 bit 7 - 0 */
  {1360, 768, 60, 85, 48, 47700, 85500},
  {1440, 900, 60, 16, 10, 55500, 88750, 1},
  {1440, 900, 60, 16, 10, 65300, 121750},
  {1440, 900, 75, 16, 10, 82300, 156000},
  {1440, 900, 85, 16, 10, 93900, 179500},
  {1400, 1050, 60, 4, 3, 64700, 101000, 1},
  {1400, 1050, 60, 4, 3, 65300, 121750},
  {1400, 1050, 75, 4, 3, 82300, 156000},
  /* 0x09 bit 7 - 0 */
  {1400, 1050, 85, 4, 3, 93900, 179500},
  {1680, 1050, 60, 16, 10, 64700, 119000, 1},
  {1680, 1050, 60, 16, 10, 65300, 146250},
  {1680, 1050, 75, 16, 10, 82300, 187000},
  {1680, 1050, 85, 16, 10, 93900, 214750},
  {1600, 1200, 60, 4, 3, 75000, 162000},
  {1600, 1200, 65, 4, 3, 81300, 175500},
  {1600, 1200, 70, 4, 3, 87500, 189000},
  /* 0x0a bit 7 - 0 */
  {1600, 1200, 75, 4, 3, 93800, 202500},
  {1600, 1200, 85, 4, 3, 106300, 229500},
  {1792, 1344, 60, 4, 3, 83600, 204750},
  {1792, 1344, 75, 4, 3, 106300, 261000},
  {1856, 1392, 60, 4, 3, 86300, 218250},
  {1856, 1392, 75, 4, 3, 112500, 288000},
  {1920, 1200, 60, 16, 10, 74000, 154000, 1},
  {1920, 1200, 60, 16, 10, 74600, 193250},
  /* 0x0b bit 7 - 4 */
  {1920, 1200, 75, 16, 10, 94000, 245250},
  {1920, 1200, 85, 16, 10, 107200, 281250},
  {1920, 1440, 60, 4, 3, 90000, 234000},
  {1920, 1440, 75, 4, 3, 112500, 297000},
};

static int
get_bit (int in, int bit)
{
    return (in & (1 << bit)) >> bit;
}

static int
get_bits (int in, int begin, int end)
{
    int mask = (1 << (end - begin + 1)) - 1;

    return (in >> begin) & mask;
}

static int
decode_vendor_and_product_identification2 (const uchar *edid, xRandrCurrentMonitor *monitor)
{
    /* Manufacturer Code */
    monitor->manufacturer_code[0]  = get_bits (edid[0x08], 2, 6);
    monitor->manufacturer_code[1]  = get_bits (edid[0x08], 0, 1) << 3;
    monitor->manufacturer_code[1] |= get_bits (edid[0x09], 5, 7);
    monitor->manufacturer_code[2]  = get_bits (edid[0x09], 0, 4);
    monitor->manufacturer_code[3]  = '\0';
	
    monitor->manufacturer_code[0] += 'A' - 1;
    monitor->manufacturer_code[1] += 'A' - 1;
    monitor->manufacturer_code[2] += 'A' - 1;
	//printf("manufacturer_code:%s\n", monitor->manufacturer_code);
    /* Product Code */
    monitor->product_code = edid[0x0b] << 8 | edid[0x0a];
	//printf("product_code:%d\n", monitor->product_code);

    return TRUE;
}

#if 0
static int
decode_detailed_resolution (const uchar *edid, xRandrCurrentMonitor *monitor, int *idx)
{
    int pixclk_khz;
    int hbl, vbl;
    int i;

    if (edid[0] == 0 && edid[1] == 0) 
    {
        if(edid[3] == 0xF7) 
        {
            printf("Established timings III:\n");
	    for (i = 0; i < 44; i++) {
	        if (edid[6 + i / 8] & (1 << (7 - i % 8))) {
                    monitor->modes[*idx].width = established_timings3[i].x;
                    monitor->modes[*idx].height = established_timings3[i].y;
                    monitor->modes[*idx].frequency = established_timings3[i].refresh;
                    *idx++;
		    printf("  %dx%d@%dHz %s%u:%u HorFreq: %d Hz Clock: %.3f MHz\n",
		       established_timings3[i].x,
		       established_timings3[i].y, established_timings3[i].refresh,
		       established_timings3[i].rb ? "RB " : "",
		       established_timings3[i].ratio_w, established_timings3[i].ratio_h,
		       established_timings3[i].hor_freq_hz,
		       established_timings3[i].pixclk_khz / 1000.0);
	         }
	     }
         }  
	 return 1;
    }

    monitor->modes[*idx].width = (edid[2] + ((edid[4] & 0xF0) << 4));
    monitor->modes[*idx].height = (edid[5] + ((edid[7] & 0xF0) << 4));
    pixclk_khz = (edid[0] + (edid[1] << 8)) * 10;
    hbl = (edid[3] + ((edid[4] & 0x0F) << 8));
    vbl = (edid[6] + ((edid[7] & 0x0F) << 8));
    monitor->modes[*idx].frequency = (pixclk_khz * 1000) / ((monitor->modes[*idx].width + hbl) * (monitor->modes[*idx].height + vbl));
    *idx = *idx+1;

    return 1;	
}
#endif

static int
decode_resolution (const uchar *edid, xRandrCurrentMonitor *monitor)
{
    int i, j, idx;
    int hactive, hblank, vactive, vblank, pixclk, pixclk_khz, refresh;
    static const Timing established[][8] =
    {
	{
	    { 800, 600, 60 },
	    { 800, 600, 56 },
	    { 640, 480, 75 },
	    { 640, 480, 72 },
	    { 640, 480, 67 },
	    { 640, 480, 60 },
	    { 720, 400, 88 },
	    { 720, 400, 70 }
	},
	{
	    { 1280, 1024, 75 },
	    { 1024, 768, 75 },
	    { 1024, 768, 70 },
	    { 1024, 768, 60 },
	    { 1024, 768, 87 },
	    { 832, 624, 75 },
	    { 800, 600, 75 },
	    { 800, 600, 72 }
	},
	{
	    { 0, 0, 0 },
	    { 0, 0, 0 },
	    { 0, 0, 0 },
	    { 0, 0, 0 },
	    { 0, 0, 0 },
	    { 0, 0, 0 },
	    { 0, 0, 0 },
	    { 1152, 870, 75 }
	},
    };

    idx = 0;
    //Parse for Detailed Timing Descriptors...
    for (i = 0x36; i < 0x7E; i += 0x12) 
    { //read through descriptor blocks...
        if ((edid[i] != 0x00) && (edid[i+1] != 0x00)) 
        { //a dtd
	    hactive = edid[i+2] + ((edid[i+4] & 0xf0) << 4);
            hblank = edid[i+3] + ((edid[i+4] & 0x0f) << 8);
	    vactive = edid[i+5] + ((edid[i+7] & 0xf0) << 4);
	    vblank = edid[i+6] + ((edid[i+7] & 0x0f) << 8);
            pixclk = (edid[i+1] << 8) | (edid[i]);
            pixclk_khz = pixclk * 10;
            refresh = (pixclk_khz * 1000) / ((hactive + hblank) * (vactive + vblank));
	
            monitor->modes[idx].width = hactive;
	    monitor->modes[idx].height = vactive;
	    monitor->modes[idx].frequency = refresh;
	    idx++;	
	}
    }
    //Standard Timing Descriptors...
    for (i = 0; i < 8; i++)
    {
	int first = edid[0x26 + 2 * i];
	int second = edid[0x27 + 2 * i];

	if (first != 0x01 && second != 0x01)
	{
	    int w = 8 * (first + 31);
	    int h;

	    switch (get_bits (second, 6, 7))
	    {
	    case 0x00: h = (w / 16) * 10; break;
	    case 0x01: h = (w / 4) * 3; break;
	    case 0x02: h = (w / 5) * 4; break;
	    case 0x03: h = (w / 16) * 9; break;
	    }

	    monitor->modes[idx].width = w;
	    monitor->modes[idx].height = h;
	    monitor->modes[idx].frequency = get_bits (second, 0, 5) + 60;
	    idx++;	
	}
    }
    //Established Timing Descriptors...
    for (i = 0; i < 3; ++i)
    {
	for (j = 0; j < 8; ++j)
	{
	    int byte = edid[0x23 + i];

	    if (get_bit (byte, j) && established[i][j].frequency != 0)
	    {
		monitor->modes[idx] = established[i][j];
		idx++;
	    }
	}
    }
    monitor->nmode = idx;
	/*for(i=0; i<monitor->nmode; i++)
	{
		printf("%dx%d@%d\n", monitor->modes[i].width, monitor->modes[i].height, monitor->modes[i].frequency);
	}*/
    return TRUE;
}

unsigned char *
extract_edid(int fd)
{
    char *ret = NULL;
    char *start, *c;
    unsigned char *out = NULL;
    int state = 0;
    int lines = 0;
    int i;
    int out_index = 0;
    int len, size;

    ret = malloc(1024);
    size = 1024;
    len = 0;
    for (;;) {
	i = read(fd, ret + len, size - len);
	if (i < 0) {
	    free(ret);
	    return 0;
	}
	if (i == 0)
	    break;
	len += i;
	if (len == size)
	    ret = realloc(ret, size + 1024);
    }

    /* Look for xrandr --verbose output (8 lines of 16 hex bytes) */
    if ((start = strstr(ret, "EDID_DATA:")) != NULL) {
	const char indentation1[] = "                ";
	const char indentation2[] = "\t\t";
	const char *indentation;
	unsigned char *out;
	char *s;

	out = malloc(128);
	if (out == NULL)
	    return NULL;

	for (i = 0; i < 8; i++) {
	    int j;

	    /* Get the next start of the line of EDID hex. */
	    s = strstr(start, indentation = indentation1);
	    if (!s)
		s = strstr(start, indentation = indentation2);
	    if (s == NULL) {
		free(ret);
		free(out);
		return NULL;
	    }
	    start = s + strlen(indentation);

	    c = start;
	    for (j = 0; j < 16; j++) {
		char buf[3];
		/* Read a %02x from the log */
		if (!isxdigit(c[0]) || !isxdigit(c[1])) {
		    free(ret);
		    free(out);
		    return NULL;
		}
		buf[0] = c[0];
		buf[1] = c[1];
		buf[2] = 0;
		out[out_index++] = strtol(buf, NULL, 16);
		c += 2;
	    }
	}

	free(ret);
	return out;
    }

    /* wait, is this a log file? */
    for (i = 0; i < 8; i++) {
	if (!isascii(ret[i])) {
	    //edid_lines = len / 16;
	    return (unsigned char *)ret;
	}
    }

    /* I think it is, let's go scanning */
    if (!(start = strstr(ret, "EDID (in hex):")))
	return (unsigned char *)ret;
    if (!(start = strstr(start, "(II)")))
	return (unsigned char *)ret;

    for (c = start; *c; c++) {
	if (state == 0) {
	    /* skip ahead to the : */
	    if (!(c = strstr(c, ": \t")))
		break;
	    /* and find the first number */
	    while (!isxdigit(c[1]))
		c++;
	    state = 1;
	    lines++;
	    out = realloc(out, lines * 16);
	} else if (state == 1) {
	    char buf[3];
	    /* Read a %02x from the log */
	    if (!isxdigit(*c)) {
		state = 0;
		continue;
	    }
	    buf[0] = c[0];
	    buf[1] = c[1];
	    buf[2] = 0;
	    out[out_index++] = strtol(buf, NULL, 16);
	    c++;
	}
    }

    //edid_lines = lines;

    free(ret);

    return out;
}

int
decode_edid2 (const unsigned char *edid, 
			xRandrCurrentMonitor *monitor)
{
    if (decode_vendor_and_product_identification2 (edid, monitor)
	/*&& decode_detailed_resolution (edid + 0x36, monitor, &idx)
	&& decode_resolution (edid, monitor)*/)
    {
	return 1;
    }
    else
    {
	return 0;
    }
}
